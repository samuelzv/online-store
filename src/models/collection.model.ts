const Datastore = require('nedb');

export default class CollectionModel {
  collection: any;

  constructor(options) {
    this.collection = new Datastore(Object.assign({}, options, { autoload: true }));
  }

  find(query:any = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.collection.find({}).sort({}).exec((error, results) => {
        if (error) {
          return reject(error);
        }
        return resolve(results);
      });
    });
  }

}
