const path = require('path');
import CollectionModel from './collection.model';

export class BrandsCollectionModel {
  brandsCollection: CollectionModel;

  constructor() {
    this.brandsCollection = new CollectionModel({ filename: path.join(__dirname,'../db-files/brands.db')});
  }

  find(query: any = {}): Promise<any> {
    return this.brandsCollection.find(query);
  }

}
