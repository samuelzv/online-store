const path = require('path');
import CollectionModel from './collection.model';

export class ClothesCollectionModel {
  clothesCollection: any;

  constructor() {
    this.clothesCollection = new CollectionModel({ filename: path.join(__dirname,'../db-files/clothes.db')});
  }

  find(query: any = {}): Promise<any> {
    return this.clothesCollection.find(query)
  }

}
