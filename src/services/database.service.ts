import { BrandsCollectionModel } from "../models/brands-collection.model";
import { ClothesCollectionModel } from "../models/clothes-collection.model";

export const brandsCollection: BrandsCollectionModel = new BrandsCollectionModel();
export const clothesCollection: ClothesCollectionModel = new ClothesCollectionModel();

