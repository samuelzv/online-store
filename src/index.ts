import app from './app';
const port = process.env.PORT || 3000;

app.listen(port, (err) => {
  if (err) {
    console.log(err);
  }
  console.log(`Running on port: ${port}`);
});

module.exports = app;


