import * as express from 'express';
import * as lodash from 'lodash';
import { brandsCollection, clothesCollection } from './../services/database.service';


export function getRoutes(): any {
  const router = express.Router();

  router.get('/', (req, res) => {
    let query: string = req.query.search || "";

    Promise.all([brandsCollection.find(), clothesCollection.find()])
      .then((results)=> {
        const [brands, clothes] = results;
        return [brands.map((brand)=>brand.name), clothes.map((clothe)=>clothe.name)];
      })
      .then((results) => {
        const [brands, clothes] = results;
        const brandsAndClothes = lodash.union(brands, clothes);
        let highlightedResult = query;

        brandsAndClothes.forEach((name) => {
          const highlighted = brands.indexOf(name) >= 0 ? `<b>${name}</b>` : `<i>${name}</i>`;
          highlightedResult = highlightedResult.replace(new RegExp(name, 'gi'), highlighted);
        });

        res.send(highlightedResult);
      })

  });


  return router;
}



