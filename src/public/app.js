"use strict";

function convertString(string, divId) {
  $.ajax({
    url: "/store?search=" + string,
    type: 'GET',
    success: function(data){
      $('#' + divId).html(data);
    },
    error: function(data) {
      $('#' + divId).html("Error: " + data.status + " - " + data.statusText);
    }
  });
}

function performSearch() {
  convertString($('#searchString').val(), "result");
}