import * as express from 'express';
import * as path from 'path';
import * as bodyParser  from 'body-parser';


import { getRoutes as getStoreRoutes } from './controllers/store.controller';

class App {
  public express: any;

  constructor () {

    this.express = express();
    // todo remove body parser
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: true }));

    this.express.use(express.static(path.join(__dirname, 'public')));
    this.express.use('/store', getStoreRoutes());
  }
}

export default new App().express;
